--
-- PostgreSQL database dump
--

-- Dumped from database version 10.7
-- Dumped by pg_dump version 10.7

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: recipes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.recipes (
    id integer NOT NULL,
    parse_date timestamp without time zone NOT NULL,
    name text NOT NULL,
    author text NOT NULL,
    ingredients text NOT NULL,
    cooking_time text,
    energy_value text,
    favourited integer NOT NULL,
    liked integer NOT NULL,
    disliked integer NOT NULL,
    cuisine text NOT NULL
)
PARTITION BY LIST (cuisine);


ALTER TABLE public.recipes OWNER TO postgres;

--
-- Name: recipes_1; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.recipes_1 PARTITION OF public.recipes
FOR VALUES IN ('tayskaya');


ALTER TABLE public.recipes_1 OWNER TO postgres;

--
-- Name: recipes_2; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.recipes_2 PARTITION OF public.recipes
FOR VALUES IN ('yaponskaya');


ALTER TABLE public.recipes_2 OWNER TO postgres;

--
-- Name: recipes_keys; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.recipes_keys
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.recipes_keys OWNER TO postgres;

--
-- Name: recipes_keys; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.recipes_keys OWNED BY public.recipes.id;


--
-- Name: steps; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.steps (
    id integer NOT NULL,
    parse_date timestamp without time zone NOT NULL,
    recipe_id integer NOT NULL,
    step_number integer NOT NULL,
    step_description text NOT NULL,
    cuisine text NOT NULL
)
PARTITION BY LIST (cuisine);


ALTER TABLE public.steps OWNER TO postgres;

--
-- Name: steps_1; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.steps_1 PARTITION OF public.steps
FOR VALUES IN ('tayskaya');


ALTER TABLE public.steps_1 OWNER TO postgres;

--
-- Name: steps_2; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.steps_2 PARTITION OF public.steps
FOR VALUES IN ('yaponskaya');


ALTER TABLE public.steps_2 OWNER TO postgres;

--
-- Name: steps_keys; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.steps_keys
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.steps_keys OWNER TO postgres;

--
-- Name: steps_keys; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.steps_keys OWNED BY public.steps.id;


--
-- Name: recipes id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipes ALTER COLUMN id SET DEFAULT nextval('public.recipes_keys'::regclass);


--
-- Name: recipes_1 id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipes_1 ALTER COLUMN id SET DEFAULT nextval('public.recipes_keys'::regclass);


--
-- Name: recipes_2 id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipes_2 ALTER COLUMN id SET DEFAULT nextval('public.recipes_keys'::regclass);


--
-- Name: steps id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.steps ALTER COLUMN id SET DEFAULT nextval('public.steps_keys'::regclass);


--
-- Name: steps_1 id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.steps_1 ALTER COLUMN id SET DEFAULT nextval('public.steps_keys'::regclass);


--
-- Name: steps_2 id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.steps_2 ALTER COLUMN id SET DEFAULT nextval('public.steps_keys'::regclass);


--
-- Data for Name: recipes_1; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.recipes_1 (id, parse_date, name, author, ingredients, cooking_time, energy_value, favourited, liked, disliked, cuisine) FROM stdin;
\.


--
-- Data for Name: recipes_2; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.recipes_2 (id, parse_date, name, author, ingredients, cooking_time, energy_value, favourited, liked, disliked, cuisine) FROM stdin;
\.


--
-- Data for Name: steps_1; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.steps_1 (id, parse_date, recipe_id, step_number, step_description, cuisine) FROM stdin;
\.


--
-- Data for Name: steps_2; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.steps_2 (id, parse_date, recipe_id, step_number, step_description, cuisine) FROM stdin;
\.


--
-- Name: recipes_keys; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.recipes_keys', 306, true);


--
-- Name: steps_keys; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.steps_keys', 1589, true);


--
-- Name: recipes_1_parse_date_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX recipes_1_parse_date_idx ON public.recipes_1 USING btree (parse_date);


--
-- Name: recipes_2_parse_date_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX recipes_2_parse_date_idx ON public.recipes_2 USING btree (parse_date);


--
-- Name: steps_1_parse_date_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX steps_1_parse_date_idx ON public.steps_1 USING btree (parse_date);


--
-- Name: steps_2_parse_date_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX steps_2_parse_date_idx ON public.steps_2 USING btree (parse_date);


--
-- PostgreSQL database dump complete
--

