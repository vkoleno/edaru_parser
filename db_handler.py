import pg8000
import os


class DbHandler:

    def __init__(self):
        self.user = os.getenv('PGUSER')
        self.pwd = os.getenv('PGPWD')
        self.port = int(os.getenv('PGPORT')) or 5432
        self.db = os.getenv('PGDB')
        self.conn = pg8000.connect(user=self.user, password=self.pwd, port=self.port, database=self.db)

    def insert_recipe_data(self, parse_date, name, author, ingredients, cooking_time, energy_value, favourited, liked,
                           disliked, cuisine):
        sql = "INSERT INTO recipes " \
              "(parse_date, name, author, ingredients, cooking_time, energy_value, favourited, liked, disliked, cuisine) " \
              "VALUES " \
              "(TIMESTAMP '{}', '{}', '{}', '{}', '{}', '{}', {}, {}, {}, '{}') " \
              "RETURNING id;"\
            .format(parse_date, name, author, ingredients, cooking_time, energy_value, favourited, liked, disliked, cuisine)

        with self.conn.cursor() as cursor:
            cursor.execute(sql)
            self.conn.commit()
            recipe_id = cursor.fetchone()

        return recipe_id

    def insert_steps_data(self, parse_date, recipe_id, step_number, step_description, cuisine):

        sql = "INSERT INTO steps (parse_date, recipe_id, step_number, step_description, cuisine) " \
              "VALUES ('{}', {}, {}, '{}', '{}')".format(parse_date, recipe_id, step_number, step_description, cuisine)

        with self.conn.cursor() as cursor:
            cursor.execute(sql)
            self.conn.commit()
