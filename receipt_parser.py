import requests
import json
import asyncio
import aiohttp
from sys import argv
from datetime import datetime
from bs4 import BeautifulSoup
from db_handler import DbHandler


receipt_kinds = ['vypechka-deserty']
cuisines = ['yaponskaya', 'tayskaya']

base_url = 'https://eda.ru'
receipts_list_href = '/recepty/{receipt_kind}/{cuisine}-kuhnya'

db = DbHandler()


def get_cuisine_recipes_page(cuisine=None):

    # if cuisine not specified we need to prepare urls to parse for all expected cusine
    cuisine_urls = {}
    if cuisine is None:
        for kind in receipt_kinds:
            for cuisine in cuisines:
                url = (base_url + receipts_list_href).format(receipt_kind=kind, cuisine=cuisine)
                cuisine_urls[cuisine] = url
    elif cuisine not in cuisines:
        raise ValueError('Cuisine not known')
    else:
        url = (base_url + receipts_list_href).format(receipt_kind=receipt_kinds[0], cuisine=cuisine)
        cuisine_urls[cuisine] = url

    # page with recipes list loaded dynamically with js. When there are no available recipes the button to load more is
    # hidden. So when it happened, we can stop parse urls to the recipes pages. Also I would like to keep cuisine
    # tracking for partitioning data between db tables depending on it
    receipts_urls_by_cuisine = dict()
    for cuisine, url in cuisine_urls.items():
        receipts_urls = []
        page_count = 1
        response = requests.get(url)
        soup = BeautifulSoup(response.text, 'lxml')
        show_more_btn = soup.find('div', class_='js-load-more-btn')
        receipts_urls.extend([a.attrs['href'] for a in soup.select('div[class="horizontal-tile__content"] > h3 > a')])
        while 'display: none' not in show_more_btn.attrs['style']:
            page_count += 1
            response = requests.get(url + '?page={}'.format(page_count))
            soup = BeautifulSoup(response.text, 'lxml')
            show_more_btn = soup.find("div", attrs={'class': ['js-load-more-btn', 'js-load-more-btn preloader__load-more']})
            receipts_urls.extend([a.attrs['href'] for a in soup.select('div[class="horizontal-tile__content"] > h3 > a')])

        # links to some receipts has link to some /edaafisha page, filtering these links out
        receipts_urls = [url for url in receipts_urls if receipt_kinds[0] in url]
        receipts_urls_by_cuisine[cuisine] = receipts_urls
    return receipts_urls_by_cuisine


async def get_recipe_page_body(url):

    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            body = await response.text()

    return body


async def get_recipe_data(urls):
    for cuisine in urls:
        futures = [get_recipe_page_body(base_url + url) for url in urls[cuisine]]
        for future in asyncio.as_completed(futures):
            parse_date = datetime.now().strftime("%Y-%m-%d %H:%M")
            rec_response = await future
            soup = BeautifulSoup(rec_response, 'lxml')
            receipt_name = soup.find('h1', class_='recipe__name').text.strip()
            author = soup.find('p', class_='author-name').text.split(':')[1].strip()

            ingredients = dict()
            portions_set = int(soup.find('input', class_='portions-control__count').attrs['value'])
            ingredients_container = soup.select('div[class="ingredients-list__content"]')[0]
            for ingredient in ingredients_container.contents:
                if ingredient != '\n':
                    ingr_raw = json.loads(ingredient.attrs['data-ingredient-object'])
                    ingredients[ingr_raw['name']] = ingr_raw['amount']
                    ingredients['portions'] = portions_set
            ingredients_json = json.dumps(ingredients, ensure_ascii=False)

            # cooking_time may be missing
            try:
                cooking_time = soup.select('span[class="timer"] + span')[0].text
            except IndexError:
                cooking_time = None

            # energy_value may be missing
            energy_value = {}
            try:
                nutrition_lis = [li for li in soup.find('ul', class_='nutrition__list').contents if li != '\n']
                for li in nutrition_lis:
                    nutr_name = li.find('p', class_='nutrition__name').text
                    nutr_value = li.find('p', class_='nutrition__weight').text
                    nutr_measure = li.find('p', class_='nutrition__percent').text
                    energy_value[nutr_name] = ' '.join([nutr_value, nutr_measure])
            except AttributeError:
                energy_value[receipt_name] = None
            energy_value_json = json.dumps(energy_value, ensure_ascii=False)

            favourited = soup.find('span', class_='counter').text

            liked = soup.find('span', class_='js-likes-like-counter').text
            disliked = soup.find('span', class_='js-likes-dislike-counter').text

            steps = {}
            steps_list = soup.find_all('span', class_='instruction__description')
            for step in steps_list:
                step_number = int(step.find('span').text.strip(' .'))
                step_action = step.contents[-1].strip()
                steps[step_number] = step_action

            # using sequence of ids in recipes table to keep connection between steps and recipe
            recipe_id = db.insert_recipe_data(parse_date, receipt_name, author, ingredients_json, cooking_time,
                                              energy_value_json, favourited, liked, disliked, cuisine)[0]
            for step in steps:
                db.insert_steps_data(parse_date, recipe_id, step, steps[step], cuisine)


# added this function just because by requirements as I understood parsing should be incapsulated in single function
def parse_recipes(cuisine=None):
    urls = get_cuisine_recipes_page(cuisine)
    ioloop = asyncio.get_event_loop()
    ioloop.run_until_complete(get_recipe_data(urls))
    ioloop.close()


if __name__ == '__main__':
    if len(argv) == 2:
        cuisine = argv[1]
    else:
        cuisine = None
    parse_recipes(cuisine)
